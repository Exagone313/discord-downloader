# Discord Downloader

Downloads latest Discord Canary and runs it.

Special thanks to simoniz0r's [Discord-AppImage](https://github.com/simoniz0r/Discord-AppImage) project for the input on how to retrieve latest Canary.

## Requirements

* curl
* jq
